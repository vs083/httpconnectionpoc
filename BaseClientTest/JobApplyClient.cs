﻿using LiveCareer.Configuration.Base;
using LiveCareer.Services.Client.Base.Enterprise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BaseClientTest
{
    public abstract class SampleClientBase : LiveCareer.Services.Client.Base.Enterprise.Client
    {

        protected SampleClientBase(string serviceName) : base(serviceName)
        {
        }


        protected SampleClientBase(string serviceName, string serviceURL, string appCD, string sourceAppCD, string authServiceURL, string clientSecretKey,
            RetrySettings waitAndRetrySettings, RetrySettings circuitBreakerSettings, TelemetrySettings telemetrySettings, int requestTimeout) : base(serviceName, serviceURL, appCD, sourceAppCD, authServiceURL, clientSecretKey, waitAndRetrySettings, circuitBreakerSettings, telemetrySettings, requestTimeout)
        {
        }

        public SampleClientBase(LiveCareerCommonWeb config, string serviceName) : base(config, serviceName)
        {
        }

        public abstract Task<HttpResponseMessage> GetAsync();

        public abstract HttpResponseMessage Get();

    }

    public class SampleClient : SampleClientBase
    {
        #region Constructors
        public SampleClient(string serviceName) : base(serviceName)
        {
        }


        public SampleClient(string serviceName, string serviceURL, string appCD, string sourceAppCD, string authServiceURL, string clientSecretKey,
            RetrySettings waitAndRetrySettings, RetrySettings circuitBreakerSettings, TelemetrySettings telemetrySettings, int requestTimeout) : base(serviceName, serviceURL, appCD, sourceAppCD, authServiceURL, clientSecretKey, waitAndRetrySettings, circuitBreakerSettings, telemetrySettings, requestTimeout)
        {
        }

        public SampleClient(LiveCareerCommonWeb config, string serviceName) : base(config, serviceName)
        {
        }

        public override async Task<HttpResponseMessage> GetAsync()
        {
            return await base.GetResponseAsync<string>("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1", HttpMethod.Get, null, null, false);
        }

        public override HttpResponseMessage Get()
        {
            return base.GetResponse<string>("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1", HttpMethod.Get, null, null, false);
        }
        #endregion
    }
}
