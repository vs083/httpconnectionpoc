﻿using LiveCareer.Services.Client.Base.Enterprise;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BaseClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL();
            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL2();
            ServicePoint_ConnectionLeaseTimout_Test();
        }

        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL()
        {
            var httpSite = "http://api-test-jobapply.livecareer.com";

            var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
            if (servicePoint != null)
            {
                servicePoint.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes(2).TotalMilliseconds;
                servicePoint.ConnectionLimit = 1;
                LogConsoleMessage(servicePoint);
            }

            int numberRequests = 4;
            int timeout = 2000;

            var client = new SampleClient("test1", httpSite, "ADP_QUA_W_COR", "RSMNA",
                    "https://test-auth.livecareer.com/oauth/token", "TWFkYW4gVGhlYXRyZXMgTHRk",
                    new RetrySettings() { Enabled = false, WaitPeriod = 5000, FailureThreshold = 2 },
                    new RetrySettings() { Enabled = false, FailureThreshold = 10, WaitPeriod = 200 },
                    new TelemetrySettings() { Enabled = false }, 2000);


            var task1 = ConnectionLimitTestAsync(client, numberRequests, timeout);
            var task2 = ConnectionLimitTestAsync(client, numberRequests, timeout);
            //Task.WaitAll(new Task[] { task1 });
            Task.WaitAll(new Task[] { task1, task2 });

            servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            LogConsoleMessage(servicePoint);
        }

        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL2()
        {
            var httpSite = "http://api-test-jobapply.livecareer.com";

            //var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("https://test-auth.livecareer.com/oauth/token"));
            //LogConsoleMessage(servicePoint);
            //ServicePointManager.DefaultConnectionLimit = 1;
            //servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
            //servicePoint.ConnectionLimit = 1;
            //LogConsoleMessage(servicePoint);

            //var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
            //if (servicePoint != null)
            //{

            //    servicePoint.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes(2).TotalMilliseconds;
            //    servicePoint.ConnectionLimit = 1;

            //    Console.WriteLine("Url : " + servicePoint.Address.DnsSafeHost + ", HashCode : " + servicePoint.GetHashCode() + ", Nagle : " + servicePoint.UseNagleAlgorithm + ", Connections:" + servicePoint.CurrentConnections);
            //    Console.WriteLine("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", ThreadId : " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            //}

            int numberRequests = 4;
            int timeout = 2000;

            var client = new SampleClient("test1", httpSite, "ADP_QUA_W_COR", "RSMNA",
                    "https://test-auth.livecareer.com/oauth/token", "TWFkYW4gVGhlYXRyZXMgTHRk",
                    new RetrySettings() { Enabled = false, WaitPeriod = 5000, FailureThreshold = 2 },
                    new RetrySettings() { Enabled = false, FailureThreshold = 10, WaitPeriod = 200 },
                    new TelemetrySettings() { Enabled = false }, timeout);

            //var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
            //if (servicePoint != null)
            //{

            //    Console.WriteLine("Url : " + servicePoint.Address.DnsSafeHost + ", HashCode : " + servicePoint.GetHashCode() + ", Nagle : " + servicePoint.UseNagleAlgorithm + ", Connections:" + servicePoint.CurrentConnections);
            //    Console.WriteLine("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", ThreadId : " + System.Threading.Thread.CurrentThread.ManagedThreadId);

            //    servicePoint.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes(2).TotalMilliseconds;
            //    servicePoint.ConnectionLimit = 1;

            //    Console.WriteLine("Url : " + servicePoint.Address.DnsSafeHost + ", HashCode : " + servicePoint.GetHashCode() + ", Nagle : " + servicePoint.UseNagleAlgorithm + ", Connections:" + servicePoint.CurrentConnections);
            //    Console.WriteLine("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", ThreadId : " + System.Threading.Thread.CurrentThread.ManagedThreadId);
            //}


            var task1 = Task.Factory.StartNew(() => ConnectionLimitTest(client, numberRequests, timeout));
            var task2 = Task.Factory.StartNew(() => ConnectionLimitTest(client, numberRequests, timeout));

            Task.WaitAll(new Task[] { task1, task2 });
            //Task.WaitAll(new Task[] { task1 });

            var servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            LogConsoleMessage(servicePoint);
        }

        private static void ServicePoint_ConnectionLeaseTimout_Test()
        {
            var httpSite = "http://api-test-jobapply.livecareer.com";
            var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
            if (servicePoint != null)
            {
                LogConsoleMessage(servicePoint);
            }

            int numberRequests = 2;
            int timeout = 2000;

            var client = new SampleClient("test1", httpSite, "ADP_QUA_W_COR", "RSMNA",
                    "https://test-auth.livecareer.com/oauth/token", "TWFkYW4gVGhlYXRyZXMgTHRk",
                    new RetrySettings() { Enabled = false, WaitPeriod = 5000, FailureThreshold = 2 },
                    new RetrySettings() { Enabled = false, FailureThreshold = 10, WaitPeriod = 200 },
                    new TelemetrySettings() { Enabled = false }, 2000);


            var task1 = ConnectionLimitTestAsync(client, numberRequests, timeout);
            task1.Wait();

            Task.Delay(1000 * 65).Wait();

            var task2 = ConnectionLimitTestAsync(client, numberRequests, timeout);
            task2.Wait();

            Task.Delay(1000 * 150).Wait();

            var task3 = ConnectionLimitTestAsync(client, numberRequests, timeout);
            task3.Wait();

            servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            LogConsoleMessage(servicePoint);
        }

        private static async Task ConnectionLimitTestAsync(SampleClient client, int numberRequests, int timeout)
        {
            Stopwatch stopwatch = null;

            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    var responseMessage = await client.GetAsync();
                    var response = await responseMessage.Content.ReadAsStringAsync();
                    var hashCode = (responseMessage.Headers.Where(x => x.Key == "HashCode") != null) ? responseMessage.Headers.FirstOrDefault(x => x.Key == "HashCode").Value.First() : "-1";
                    stopwatch.Stop();
                    var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
                    LogConsoleMessage($"End Req {i},{response},{servicePoint.GetHashCode()},{hashCode},{stopwatch.ElapsedMilliseconds}");
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }
        }



        private static void ConnectionLimitTest(SampleClient client, int numberRequests, int timeout)
        {
            Stopwatch stopwatch = null;

            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    var responseMessage = client.Get();
                    var response = responseMessage.Content.ReadAsStringAsync().Result;
                    stopwatch.Stop();
                    var servicePoint = ServicePointManager.FindServicePoint(new System.Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity=1"));
                    LogConsoleMessage($"End Req {i},{response},{servicePoint.GetHashCode()},{stopwatch.ElapsedMilliseconds}");
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    LogConsoleMessage(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }
        }

        public static void LogConsoleMessage(string message)
        {
            Console.WriteLine(DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss.fff") + " ===> " + message);
        }

        public static void LogConsoleMessage(ServicePoint servicePoint)
        {
            LogConsoleMessage("Url: " + servicePoint.Address.DnsSafeHost + ", HashCode : " + servicePoint.GetHashCode() + ", Connections:" + servicePoint.CurrentConnections);
            LogConsoleMessage("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", ConnectionLeaseTimeout : " + servicePoint.ConnectionLeaseTimeout);
        }
    }
}
