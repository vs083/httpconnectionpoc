﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Utility;
using System.Net.Http;

namespace HTTPConnectionPoC
{
    class Program
    {
        static void Main(string[] args)
        {

            ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL();

            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_With_TaskRun_FAIL();

            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS();

            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS_2();

            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS_3();

            //UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_GetAsync_FAIL();

            //UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_SendAsync_FAIL();

            //UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_PASS();

            //UsingHttpClient_AsyncIO_With_TaskWaitAll();

            //ServicePoint_ConnectionLimit_Test_HTTPWebRequest_Async_PASS();

            //UsingIndependentHttpClient_AsyncIO_With_TaskWaitAll_Async_PASS();

            //BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout();
            //Task.Delay(100).Wait();
            //BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout();

            //BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout2();

            //BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout3();

            //Task.Delay(100).Wait();
            //BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout2();

            //ServicePoint_ConnectionLeaseTimout_Test();

            //BeginTest_Host_To_Host();

            //BeginTest_Host_To_Host2();

            //Console.WriteLine("Test Completed, Press any key to quit !!!");
            //Console.Read();
        }


        /// <summary>
        /// Test is supposed to fail because of shared httpclient and reduced number of connection given.
        /// It uses httpclient.getasync()
        /// </summary>
        private static void UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_GetAsync_FAIL()
        {
            int numberOfConnections = 1;

            HttpClient client = new HttpClient();

            client.Timeout = new TimeSpan(0, 0, 2);

            Stopwatch stopWatch = Stopwatch.StartNew();
            int loopCount = 4;
            var task1 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async(client, numberOfConnections, loopCount);
            var task2 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async(client, numberOfConnections, loopCount);

            Common.LogConsoleMessage("Waiting all tasks");

            Task.WaitAll(new Task[] { task1, task2 });
            stopWatch.Stop();

            Common.LogConsoleMessage($"UsingHttpClient_AsyncIO_WithLoop ends in : {stopWatch.ElapsedMilliseconds}");
        }

        /// <summary>
        /// Test is supposed to fail because of shared httpclient and reduced number of connection given.
        /// It uses httpclient.sendasync()
        /// </summary>
        private static void UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_SendAsync_FAIL()
        {
            int numberOfConnections = 1;
            HttpClient client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);

            Stopwatch stopWatch = Stopwatch.StartNew();
            int loopCount = 4;
            var task1 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_SendAsync(client, numberOfConnections, loopCount);
            var task2 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_SendAsync(client, numberOfConnections, loopCount);

            Common.LogConsoleMessage("Waiting all tasks");

            Task.WaitAll(new Task[] { task1, task2 });
            stopWatch.Stop();

            Common.LogConsoleMessage($"UsingHttpClient_AsyncIO_WithLoop ends in : {stopWatch.ElapsedMilliseconds}");
        }

        /// <summary>
        /// Test will pass because even if shared httpclient, more connections are available to issue requests.
        /// </summary>
        private static void UsingHttpClient_AsyncIO_With_TaskWaitAll_Async_PASS()
        {
            int numberOfConnections = 2;
            HttpClient client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);

            Stopwatch stopWatch = Stopwatch.StartNew();
            int loopCount = 400;
            var task1 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async(client, numberOfConnections, loopCount);
            //var task2 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async(client, numberOfConnections, loopCount);

            Common.LogConsoleMessage("Waiting all tasks");


            //Task.WaitAll(new Task[] { task1, task2 });
            Task.WaitAll(new Task[] { task1 });
            stopWatch.Stop();

            Common.LogConsoleMessage($"UsingHttpClient_AsyncIO_WithLoop ends in : {stopWatch.ElapsedMilliseconds}");
        }

        /// <summary>
        /// Run synchronous and pass
        /// </summary>
        private static void UsingHttpClient_AsyncIO_With_TaskWaitAll()
        {
            HttpClient client = new HttpClient();
            Stopwatch stopWatch = Stopwatch.StartNew();
            ServicePointManager.DefaultConnectionLimit = 1;
            int loopCount = 4;
            var task1 = Task.Run(() => ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient(client, 1, loopCount));

            Common.LogConsoleMessage("Waiting all tasks");

            ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient(client, 2, loopCount);

            task1.Wait();

            stopWatch.Stop();

            Common.LogConsoleMessage($"UsingHttpClient_AsyncIO_WithLoop ends in : {stopWatch.ElapsedMilliseconds}");
        }


        /// <summary>
        /// Test will pass because of independent httpclient, each client going to get a different connection.
        /// </summary>
        private static void UsingIndependentHttpClient_AsyncIO_With_TaskWaitAll_Async_PASS()
        {
            int numberOfConnections = 1;
            Stopwatch stopWatch = Stopwatch.StartNew();
            int loopCount = 4;
            var task1 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async2(numberOfConnections, loopCount);
            var task2 = ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async2(numberOfConnections, loopCount);

            Common.LogConsoleMessage("Waiting all tasks");


            Task.WaitAll(new Task[] { task1, task2 });
            stopWatch.Stop();

            Common.LogConsoleMessage($"UsingHttpClient_AsyncIO_WithLoop ends in : {stopWatch.ElapsedMilliseconds}");
        }

        static async Task ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async(HttpClient client, int numberOfConnections, int count)
        {
            var uri = new Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&cb=" + DateTime.Now.Ticks);
            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            Common.LogConsoleMessage($"ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
            sp.ConnectionLimit = numberOfConnections;
            sp.ConnectionLeaseTimeout = 5000;
            Common.LogConsoleMessage("Before using HttpClient APIs, after changing ConnectionLimit");
            Common.LogConsoleMessage($"ConnectionLimit (should be {numberOfConnections}): {sp.ConnectionLimit}");


            for (int i = 0; i < count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    stopwatch.Stop();
                    sp = ServicePointManager.FindServicePoint(uri);
                    Common.LogConsoleMessage($"ServicePoint HashCode : {sp.GetHashCode()}, End Req {i}, {stopwatch.ElapsedMilliseconds}");
                }
                catch (Exception ex)
                {
                    Common.LogConsoleMessage($"{i}, TimeTaken : {stopwatch.ElapsedMilliseconds }, EXCEPTION : {ex.Message},  InnerException : {ex.InnerException?.Message}");
                }
            }

            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, After using HttpClient API, ConnectionLimit : {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
        }

        static async Task ServicePoint_ConnectionLeaseTimeout_Test_Request_Using_HttpClient_Async(HttpClient client, int numberOfConnections, int count,int connectionLeaseTimeout)
        {
            var uri = new Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&cb=" + DateTime.Now.Ticks);
            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            Common.LogConsoleMessage($"ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
            sp.ConnectionLimit = numberOfConnections;
            sp.ConnectionLeaseTimeout = connectionLeaseTimeout;
            Common.LogConsoleMessage("Before using HttpClient APIs, after changing ConnectionLimit");
            Common.LogConsoleMessage($"ConnectionLimit (should be {numberOfConnections}): {sp.ConnectionLimit}");


            for (int i = 0; i < count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    stopwatch.Stop();
                    sp = ServicePointManager.FindServicePoint(uri);
                    Common.LogConsoleMessage($"ServicePoint HashCode : {sp.GetHashCode()}, End Req {i}, {stopwatch.ElapsedMilliseconds}");
                }
                catch (Exception ex)
                {
                    Common.LogConsoleMessage($"{i}, TimeTaken : {stopwatch.ElapsedMilliseconds }, EXCEPTION : {ex.Message},  InnerException : {ex.InnerException?.Message}");
                }
            }

            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, After using HttpClient API, ConnectionLimit : {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
        }

        static async Task ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_SendAsync(HttpClient client, int numberOfConnections, int count)
        {
            var uri = new Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&cb=" + DateTime.Now.Ticks);
            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            Common.LogConsoleMessage($"ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
            sp.ConnectionLimit = numberOfConnections;
            Common.LogConsoleMessage("Before using HttpClient APIs, after changing ConnectionLimit");
            Common.LogConsoleMessage($"ConnectionLimit (should be {numberOfConnections}): {sp.ConnectionLimit}");


            for (int i = 0; i < count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                try
                {
                    HttpRequestMessage requestMessage = new HttpRequestMessage();
                    requestMessage.Method = HttpMethod.Get;
                    requestMessage.RequestUri = uri;
                    await client.SendAsync(requestMessage);
                    stopwatch.Stop();
                    Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, End Req {i}, {stopwatch.ElapsedMilliseconds}");
                }
                catch (Exception ex)
                {
                    Common.LogConsoleMessage($"{i}, TimeTaken : {stopwatch.ElapsedMilliseconds }, EXCEPTION : {ex.Message},  InnerException : {ex.InnerException?.Message}");
                }
            }

            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, After using HttpClient API, ConnectionLimit : {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
        }

        static async Task ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient_Async2(int numberOfConnections, int count)
        {
            var uri = new Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&cb=" + DateTime.Now.Ticks);
            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            Common.LogConsoleMessage($"ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
            sp.ConnectionLimit = numberOfConnections;
            Common.LogConsoleMessage("Before using HttpClient APIs, after changing ConnectionLimit");
            Common.LogConsoleMessage($"ConnectionLimit (should be {numberOfConnections}): {sp.ConnectionLimit}");

            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);
            for (int i = 0; i < count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                try
                {
                    HttpResponseMessage response = await client.GetAsync(uri);
                    stopwatch.Stop();
                    Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, End Req {i}, {stopwatch.ElapsedMilliseconds}");
                }
                catch (Exception ex)
                {
                    Common.LogConsoleMessage($"{i}, TimeTaken : {stopwatch.ElapsedMilliseconds }, EXCEPTION : {ex.Message},  InnerException : {ex.InnerException?.Message}");
                }
            }

            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, After using HttpClient API, ConnectionLimit : {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
        }

        static void ServicePoint_ConnectionLimit_Test_Request_Using_HttpClient(HttpClient client, int identity, int count)
        {
            int numberOfConnections = 1;
            //var uri = new Uri("http://www.microsoft.com");
            var uri = new Uri($"http://api-test-jobapply.livecareer.com/home/test?waitTime=1&identity={identity}&cb={DateTime.Now.Ticks}");
            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            Common.LogConsoleMessage($"ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
            sp.ConnectionLimit = numberOfConnections;
            Common.LogConsoleMessage("Before using HttpClient APIs, after changing ConnectionLimit");
            Common.LogConsoleMessage($"ConnectionLimit (should be {numberOfConnections}): {sp.ConnectionLimit}");
            //var client = new HttpClient();
            //client.Timeout = new TimeSpan(0, 10, 0);
            for (int i = 0; i < count; i++)
            {
                var stopwatch = Stopwatch.StartNew();
                try
                {

                    HttpRequestMessage request = new HttpRequestMessage();
                    request.RequestUri = uri;
                    request.Method = HttpMethod.Get;
                    HttpResponseMessage response = client.SendAsync(request).Result;
                    stopwatch.Stop();
                    Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, End Req {i}, {stopwatch.ElapsedMilliseconds}");
                }
                catch (Exception ex)
                {
                    Common.LogConsoleMessage($"{i}, TimeTaken : {stopwatch.ElapsedMilliseconds }, EXCEPTION : {ex.Message},  InnerException : {ex.InnerException?.Message}");
                }
            }

            Common.LogConsoleMessage($"ThreadId : {Thread.CurrentThread.ManagedThreadId}, After using HttpClient API, ConnectionLimit : {sp.ConnectionLimit}, HashCode : {sp.GetHashCode()}");
        }

        /// <summary>
        /// will fail because of limited connections.
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FAIL()
        {
            int numberRequests = 400;
            string httpSite = "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;

            //Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimitTest(numberRequests, numberOfConnections, "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=2", timeout);
        }

        /// <summary>
        /// will pass now because of increased connection limit.
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS()
        {
            int numberRequests = 4;
            string httpSite = "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 2;

            Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimitTest(numberRequests, numberOfConnections, "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=2", timeout);
        }

        /// <summary>
        /// will pass now because of increased connection limit globally.
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS_2()
        {
            int numberRequests = 4;
            string httpSite = "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;

            ServicePointManager.DefaultConnectionLimit = 2;

            Task.Run(() => ConnectionLimitTestWithoutConnectionLimitSetOnRequest(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimitTestWithoutConnectionLimitSetOnRequest(numberRequests, numberOfConnections, "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=2", timeout);
        }

        /// <summary>
        /// will pass because of increased timeout.
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_PASS_3()
        {
            int numberRequests = 4;
            string httpSite = "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 4000;
            int numberOfConnections = 1;

            Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimitTest(numberRequests, numberOfConnections, "http://api-jobapply.livecareer.com/home/test?waitTime=1&identity=2", timeout);
        }

        /// <summary>
        /// this will also fail threads because of limited connections.
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_With_TaskRun_FAIL()
        {
            int numberRequests = 4;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;

            Stopwatch stopWatch = Stopwatch.StartNew();

            var task1 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task2 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task3 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task4 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task5 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task6 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task7 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task8 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task9 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));
            var task10 = Task.Run(() => ConnectionLimitTest(numberRequests, numberOfConnections, httpSite, timeout));

            Common.LogConsoleMessage("Waiting all tasks");

            Task.WaitAll(new Task[] { task1, task2, task3, task4, task5, task6, task7, task8, task9, task10 });
            stopWatch.Stop();

            Common.LogConsoleMessage($"ServicePoint_ConnectionLimit_Test_Request_In_Waiting_Fail_With_TaskRun ends in : {stopWatch.ElapsedMilliseconds}");

            //ConnectionLimit_Test(numberRequests, numberOfConnections, httpSite, timeout);
        }

        /// <summary>
        /// this will pass because in case of async with httpwebrequest client has to implement it's own timeout mechanism as per microsoft.
        /// https://docs.microsoft.com/en-au/dotnet/api/system.net.httpwebrequest.begingetresponse?view=netframework-4.7.1
        /// </summary>
        private static void ServicePoint_ConnectionLimit_Test_HTTPWebRequest_Async_PASS()
        {
            // No timeout magic :)
            int numberRequests = 4;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;

            var task1 = ConnectionLimit_Test_Async(numberRequests, numberOfConnections, httpSite, timeout);
            var task2 = ConnectionLimit_Test_Async(numberRequests, numberOfConnections, httpSite, timeout);

            Common.LogConsoleMessage("Waiting all tasks");

            Task.WaitAll(new Task[] { task1, task2 });
        }


        /// <summary>
        /// check in wireshark, sockets will be renewed. connection close can be seen in both request and response through fiddler.
        /// </summary>
        private static void BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout()
        {
            int numberRequests = 4;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;

            Task.Run(() => KeepAlive_Vs_ConnectionLeaseTimeout(numberRequests, numberOfConnections, httpSite, timeout));
            KeepAlive_Vs_ConnectionLeaseTimeout(numberRequests, numberOfConnections, httpSite, timeout);
        }
        /// <summary>
        /// checking in wireshark, you will see only one socket 
        /// </summary>
        private static void BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout2()
        {
            int numberRequests = 4;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 5000;
            int numberOfConnections = 1;
            Task.Run(() => KeepAlive_Vs_ConnectionLeaseTimeout2(numberRequests, numberOfConnections, httpSite, timeout));
            KeepAlive_Vs_ConnectionLeaseTimeout2(numberRequests, numberOfConnections, httpSite, timeout);
        }

        /// <summary>
        /// socket will renew because of httptimeout.
        /// </summary>
        private static void BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout3()
        {
            int numberRequests = 4;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;
            Task.Run(() => KeepAlive_Vs_ConnectionLeaseTimeout2(numberRequests, numberOfConnections, httpSite, timeout));
            KeepAlive_Vs_ConnectionLeaseTimeout2(numberRequests, numberOfConnections, httpSite, timeout);
        }

        /// <summary>
        /// Test to observe service point object creation for different host on same machine.
        /// not equals in both time.
        /// </summary>
        private static void BeginTest_Host_To_Host()
        {
            int numberRequests = 2;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 5000;
            int numberOfConnections = 1;

            var servicePoint1 = Host_To_Host(numberRequests, numberOfConnections, httpSite, timeout);
            var servicePoint2 = Host_To_Host(numberRequests, numberOfConnections, "http://vikaspedia.in/e-governance/citizen-services/apply-online-for-pan-card", timeout);
            var servicePoint3 = Host_To_Host(numberRequests, numberOfConnections, "http://api-test-jobsearch.livecareer.com/swagger/ui/index.html", timeout);

            Common.LogConsoleMessage(string.Format("{0} and {1} are {2}", servicePoint1.GetHashCode(), servicePoint2.GetHashCode(), (servicePoint1.GetHashCode() == servicePoint2.GetHashCode() ? "equal" : "not equal")));
            Common.LogConsoleMessage(string.Format("{0} and {1} are {2}", servicePoint1.GetHashCode(), servicePoint3.GetHashCode(), (servicePoint1.GetHashCode() == servicePoint3.GetHashCode() ? "equal" : "not equal")));
        }

        /// <summary>
        /// Test to observe service point object creation for different host on same machine.
        /// equals in second row only.
        /// </summary>
        private static void BeginTest_Host_To_Host2()
        {
            int numberRequests = 2;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 5000;
            int numberOfConnections = 1;

            var servicePoint1 = Host_To_Host(numberRequests, numberOfConnections, httpSite, timeout);
            var servicePoint2 = Host_To_Host(numberRequests, numberOfConnections, "http://vikaspedia.in/e-governance/citizen-services/apply-online-for-pan-card", timeout);
            var servicePoint3 = Host_To_Host(numberRequests, numberOfConnections, httpSite, timeout);

            Common.LogConsoleMessage(string.Format("{0} and {1} are {2}", servicePoint1.GetHashCode(), servicePoint2.GetHashCode(), (servicePoint1.GetHashCode() == servicePoint2.GetHashCode() ? "equal" : "not equal")));
            Common.LogConsoleMessage(string.Format("{0} and {1} are {2}", servicePoint1.GetHashCode(), servicePoint3.GetHashCode(), (servicePoint1.GetHashCode() == servicePoint3.GetHashCode() ? "equal" : "not equal")));
        }

        /// <summary>
        /// Test to observer ConnectionLimit behaviour 
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static void ConnectionLimitTest(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            Stopwatch stopwatch = null;
            string url = string.Empty;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    url = httpSite + "&cb=" + id;
                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.ServicePoint.ConnectionLimit = numberOfConnections;
                    request.ServicePoint.ConnectionLeaseTimeout = 1000 * 5;
                    request.Timeout = timeout;
                    Common.LogConsoleMessage($"ServicePoint HashCode : {request.ServicePoint.GetHashCode()}, Connections : {request.ServicePoint.CurrentConnections}");
                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }

            var servicePoint = ServicePointManager.FindServicePoint(new Uri(httpSite));
            Common.LogConsoleMessage(false, "ConnectionLimitTest", servicePoint, numberRequests);
        }

        /// <summary>
        /// Test to observer ConnectionLimit behaviour 
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static void ConnectionLimitTestWithoutConnectionLimitSetOnRequest(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            Stopwatch stopwatch = null;
            string url = string.Empty;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");
                    url = httpSite + "&cb=" + id;
                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Timeout = timeout;
                    Common.LogConsoleMessage($"ServicePoint HashCode : {request.ServicePoint.GetHashCode()}, Connections : {request.ServicePoint.CurrentConnections}");
                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Common.LogConsoleMessage(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }

            var servicePoint2 = ServicePointManager.FindServicePoint(new Uri("http://api-test-jobapply.livecareer.com/"));
            Common.LogConsoleMessage(false, "ConnectionLimit_Test", servicePoint2, numberRequests);
        }

        /// <summary>
        /// Test to observer ConnectionLimit behaviour 
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static async Task ConnectionLimit_Test_Async(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);
                    request.Timeout = timeout;
                    request.ServicePoint.ConnectionLimit = numberOfConnections;
                    // Send the request.
                    var response = await request.GetResponseAsync();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    await s.ReadToEndAsync();
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }
            var servicePoint2 = ServicePointManager.FindServicePoint(new Uri(httpSite));
            Common.LogConsoleMessage(false, "ConnectionLimit_Test", servicePoint2, numberRequests);
        }

        /// <summary>
        /// Test to demonstrate http connection remain alive even when connectionlease timeout is lower. Change keepalive & leasetimeout setting to observe behaviour
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static void KeepAlive_Vs_ConnectionLeaseTimeout(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            var servicePoint = ServicePointManager.FindServicePoint(new Uri(httpSite));
            Common.LogConsoleMessage(true, "BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout", servicePoint, numberRequests);

            servicePoint.ConnectionLimit = numberOfConnections;
            //servicePoint.ConnectionLeaseTimeout = 100;

            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);

                    request.Timeout = timeout;
                    request.KeepAlive = false;

                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }

            Common.LogConsoleMessage(false, "BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout", servicePoint, numberRequests);

        }

        /// <summary>
        /// Test to demonstrate http connection remain alive even when connectionlease timeout is lower. Change keepalive & leasetimeout setting to observe behaviour
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static void KeepAlive_Vs_ConnectionLeaseTimeout2(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            var servicePoint = ServicePointManager.FindServicePoint(new Uri(httpSite));
            Common.LogConsoleMessage(true, "BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout", servicePoint, numberRequests);

            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);
                    request.ServicePoint.ConnectionLimit = numberOfConnections;
                    request.ServicePoint.ConnectionLeaseTimeout = 1000 * 10;
                    request.Timeout = timeout;
                    request.KeepAlive = true;


                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }

            Common.LogConsoleMessage(false, "BeginTest_KeepAlive_Vs_ConnectionLeaseTimeout", servicePoint, numberRequests);

        }

        /// <summary>
        /// Connection should reset after wait... so as their settings. 
        /// </summary>
        private static void ServicePoint_ConnectionLeaseTimout_Test()
        {
            var httpSite = "http://api-test-jobapply.livecareer.com";
            int numberOfConnections = 1;
            int connectionLeaseTimeout = 2000;
            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);
            var count = 2;

            var task1 = ServicePoint_ConnectionLeaseTimeout_Test_Request_Using_HttpClient_Async(client, numberOfConnections, count, connectionLeaseTimeout);
            task1.Wait();

            Task.Delay(1000 * 125).Wait();

            var servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            if (servicePoint != null)
            {
                Common.LogConsoleMessage(servicePoint);
            }

            var task2 = ServicePoint_ConnectionLeaseTimeout_Test_Request_Using_HttpClient_Async(client, numberOfConnections, count, connectionLeaseTimeout);
            task2.Wait();

            Task.Delay(1000 * 125).Wait();

            servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            if (servicePoint != null)
            {
                Common.LogConsoleMessage(servicePoint);
            }

            var task3 = ServicePoint_ConnectionLeaseTimeout_Test_Request_Using_HttpClient_Async(client, numberOfConnections, count, connectionLeaseTimeout);
            task3.Wait();

            servicePoint = ServicePointManager.FindServicePoint(new System.Uri(httpSite));
            Common.LogConsoleMessage(servicePoint);
        }


        /// <summary>
        /// Test to demonstrate http connection remain alive even when connectionlease timeout is lower. Change keepalive & leasetimeout setting to observe behaviour
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private static ServicePoint Host_To_Host(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {
            ServicePoint servicePoint = null;

            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);
                    request.Timeout = timeout;
                    servicePoint = request.ServicePoint;
                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.LogConsoleMessage(string.Format("End Req {0},{1}", stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Console.WriteLine(string.Format("EXCEPTION for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }

            Common.LogConsoleMessage(false, "Host_To_Host", servicePoint, numberRequests);

            return servicePoint;
        }
    }
}
