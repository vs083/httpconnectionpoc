﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Utility;

namespace HttpConnection.WebApi.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            Common.ClearQueue();
            //ServicePoint_ConnectionLimit_Test_Request_In_Waiting_Fail();

            BlockAll();

            ViewBag.Title = "Home Page";

            return View(Common._logBuilder);
        }

        private static void BlockAll()
        {
            int numberRequests = 20;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 1500;

            Common.EnqueueMessage("Before beginning load, ServicePointManager : " + ServicePointManager.FindServicePoint(new Uri(httpSite)).ConnectionLimit.ToString() + " NumberOfRequest : " + numberRequests);

            List<Task> tasks = new List<Task>();
            for (int i = 0; i < numberRequests; i++)
            {
                Common.EnqueueMessage(string.Format("Going to invoke thread#{0}", i));
                var task = SendRequestAsync(httpSite, timeout, i);
                tasks.Add(task);
            }
            Task.WaitAll(tasks.ToArray());


            Common.EnqueueMessage("After ending load, ServicePointManager : " + ServicePointManager.FindServicePoint(new Uri(httpSite)).ConnectionLimit.ToString() + " NumberOfRequest : " + numberRequests);

        }

        private static async Task SendRequestAsync(string httpSite, int timeout, int counter = 0)
        {
            Common.EnqueueMessage(string.Format("Going to invoke thread#{0}", counter));
            Stopwatch stopwatch = Stopwatch.StartNew();
            string id = Guid.NewGuid().ToString("N");
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = new TimeSpan(0, 0, 0, 0, timeout);
                    await httpClient.GetStringAsync(httpSite + "&" + id);
                    stopwatch.Stop();
                    Common.EnqueueMessage(string.Format("End Request {0},{1},{2}", counter, stopwatch.ElapsedMilliseconds, id));
                }
            }
            catch (TaskCanceledException ex)
            {
                stopwatch.Stop();
                Common.EnqueueMessage(string.Format("TaskCanceledException for Request {0}: {1},{2},{3}", counter, stopwatch.ElapsedMilliseconds, id, ex.Message));
            }
            catch (AggregateException ex)
            {
                stopwatch.Stop();
                Common.EnqueueMessage(string.Format("AggregateException for Request {0}: {1},{2},{3}", counter, stopwatch.ElapsedMilliseconds, id, ex.Flatten().Message));
            }
            catch (Exception ex)
            {
                stopwatch.Stop();
                Common.EnqueueMessage(string.Format("Exception for Request {0}: {1},{2},{3}", counter, stopwatch.ElapsedMilliseconds, id, ex.Message));
                Common.EnqueueMessage(string.Format("InnerException for Request {0}: {1}", counter, ex.InnerException.Message));
            }
        }


        private void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_Fail()
        {
            int numberRequests = 8;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 5000;
            int numberOfConnections = 1;
            var task = Task.Run(() => ConnectionLimit_Test(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimit_Test(numberRequests, numberOfConnections, "https://msdn.microsoft.com/en-us/", timeout);
            task.Wait();
        }

        /// <summary>
        /// Test to observer ConnectionLimit behaviour 
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private void ConnectionLimit_Test(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {

            var servicePoint = ServicePointManager.FindServicePoint(new Uri(httpSite));
            Common.EnqueueMessage(true, "ConnectionLimit_Test", servicePoint, numberRequests);

            servicePoint.ConnectionLimit = numberOfConnections;
            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);
                    request.Timeout = timeout;

                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);

                    throw new Exception();
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    Common.EnqueueMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    Common.EnqueueMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    Common.EnqueueMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    Common.EnqueueMessage(string.Format("Exception for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }
            Common.EnqueueMessage(false, "ConnectionLimit_Test", servicePoint, numberRequests);
        }
    }
}
