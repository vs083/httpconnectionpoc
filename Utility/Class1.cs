﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utility
{
    public class Common
    {
        public static ConcurrentQueue<string> _logBuilder = new ConcurrentQueue<string>();

        public static void LogConsoleMessage(bool before, string testType, ServicePoint servicePoint, int numberRequests)
        {
            LogConsoleMessage("Setting " + (before == true ? "before" : "after") + ",  HashCode : " + servicePoint.GetHashCode());
            LogConsoleMessage("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", NumberOfRequest : " + numberRequests + ", ThreadId : " + System.Threading.Thread.CurrentThread.ManagedThreadId);
        }
        public static void LogConsoleMessage(string message)
        {
            Console.WriteLine(DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss.fff") + " ===> " + message);
        }

        public static void LogConsoleMessage(ServicePoint servicePoint)
        {
            LogConsoleMessage("Url: " + servicePoint.Address.DnsSafeHost + ", HashCode : " + servicePoint.GetHashCode() + ", Connections:" + servicePoint.CurrentConnections);
            LogConsoleMessage("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", ConnectionLeaseTimeOut : " + servicePoint.ConnectionLeaseTimeout);
        }

        public static void EnqueueMessage(bool before, string testType, ServicePoint servicePoint, int numberRequests)
        {
            EnqueueMessage("Setting " + (before == true ? "before" : "after") + ", Url : " + servicePoint.Address.DnsSafeHost + ", Expect100Continue : " + servicePoint.Expect100Continue + ", Nagle : " + servicePoint.UseNagleAlgorithm + ", SupportsPipelining:" + servicePoint.SupportsPipelining + ", Connections:" + servicePoint.CurrentConnections);
            EnqueueMessage("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", NumberOfRequest : " + numberRequests + ", ThreadId : " + System.Threading.Thread.CurrentThread.ManagedThreadId);
        }

        public static void EnqueueMessage(string message)
        {
            _logBuilder.Enqueue(Environment.NewLine + DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss.fff") + " ===> " + message);
        }

        public static void ClearQueue()
        {
            string temp;
            while (_logBuilder.TryDequeue(out temp)) { }
        }
    }
}
