﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HttpConnection.WebApi.Controllers
{
    public class PoCController : ApiController
    {
        ConcurrentQueue<string> _stringBuilder = new ConcurrentQueue<string>();


        [HttpGet]
        [Route("api/poc/test1")]
        // GET api/<controller>
        public IEnumerable<string> test1()
        {
            string temp;
            while (_stringBuilder.TryDequeue(out temp)) { }
            ServicePoint_ConnectionLimit_Test_Request_In_Waiting_Fail();
            return _stringBuilder.ToArray();
        }

        [HttpGet]
        [Route("api/poc/test1async")]
        // GET api/<controller>
        public async Task<IEnumerable<string>> test1async()
        {
            string temp;
            while (_stringBuilder.TryDequeue(out temp)) { }
            await ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FailAsync();
            return _stringBuilder.ToArray();
        }



        [HttpGet]
        [Route("api/poc/test2")]
        // GET api/<controller>
        public IEnumerable<string> test2()
        {
            string temp;
            while (_stringBuilder.TryDequeue(out temp)) { }

            // Make a request with the same scheme identifier and host fragment
            // used to create the previous ServicePoint object.
            makeWebRequest("http://msdn.microsoft.com/library/");

            makeWebRequest("http://msdn.microsoft.com/library/");

            return _stringBuilder.ToArray();
        }

        [HttpGet]
        [Route("api/poc/test3")]
        // GET api/<controller>
        public async Task<IEnumerable<string>> test3()
        {
            var task1 = Task.Run(() => OverrideTest());
            var task2 = Task.Run(() => OverrideTest());
            var task3 = Task.Run(() => OverrideTest());
            var task4 = Task.Run(() => OverrideTest());
            var task5 = Task.Run(() => OverrideTest());

            OverrideTest();

            await Task.WhenAll(new Task[] { task1, task2, task3, task4, task5 });
            return _stringBuilder.ToArray();
        }

        private async Task ServicePoint_ConnectionLimit_Test_Request_In_Waiting_FailAsync()
        {
            int numberRequests = 8;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 2000;
            int numberOfConnections = 1;
            var task = Task.Run(() => ConnectionLimit_Test(numberRequests, numberOfConnections, httpSite, timeout));
            ConnectionLimit_Test(numberRequests, numberOfConnections, httpSite, timeout);
            await task;
        }

        private void ServicePoint_ConnectionLimit_Test_Request_In_Waiting_Fail()
        {
            int numberRequests = 2;
            string httpSite = "http://api-test-jobapply.livecareer.com/home/test?waitTime=1"; // waittime=1 means wait for 1 seconds at server
            int timeout = 500;
            int numberOfConnections = 1;
            ConnectionLimit_Test(numberRequests, numberOfConnections, httpSite, timeout);
        }

        /// <summary>
        /// Test to observer ConnectionLimit behaviour 
        /// </summary>
        /// <param name="numberRequests"></param>
        /// <param name="numberOfConnections"></param>
        /// <param name="httpSite"></param>
        /// <param name="timeout"></param>
        private void ConnectionLimit_Test(int numberRequests, int numberOfConnections, string httpSite, int timeout)
        {

            var servicePoint = ServicePointManager.FindServicePoint(new Uri(httpSite));
            LogMessage(true, "ConnectionLimit_Test", servicePoint, numberRequests);

            servicePoint.ConnectionLimit = numberOfConnections;
            Stopwatch stopwatch = null;
            for (int i = 0; i < numberRequests; i++)
            {
                try
                {
                    stopwatch = Stopwatch.StartNew();
                    string id = Guid.NewGuid().ToString("N");

                    // Create the request.
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(httpSite + "&cb=" + id);
                    request.Timeout = timeout;

                    // Send the request.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream responseStream = response.GetResponseStream();
                    StreamReader s = new StreamReader(responseStream);
                    s.Close();
                    responseStream.Close();
                    response.Close();
                    LogMessage(string.Format("End Req {0},{1},{2}", i, stopwatch.ElapsedMilliseconds, id));
                }
                catch (TaskCanceledException ex)
                {
                    stopwatch.Stop();
                    LogMessage(string.Format("TaskCanceledException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (AggregateException ex)
                {
                    stopwatch.Stop();
                    LogMessage(string.Format("AggregateException for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
                catch (Exception ex)
                {
                    stopwatch.Stop();
                    LogMessage(string.Format("Exception for Request : {0},{1},{2}", stopwatch.ElapsedMilliseconds, Thread.CurrentThread.ManagedThreadId, ex.Message));
                }
            }
            LogMessage(false, "ConnectionLimit_Test", servicePoint, numberRequests);
        }

        void LogMessage(bool before, string testType, ServicePoint servicePoint, int numberRequests)
        {
            LogMessage("Setting " + (before == true ? "before" : "after") + ":");
            LogMessage("Url : " + servicePoint.Address.DnsSafeHost+ ", HashCode : " + servicePoint.GetHashCode());
            LogMessage("ServicePoint(s) : " + servicePoint.ConnectionLimit.ToString() + ", NumberOfRequest : " + numberRequests);
        }

        void LogMessage(string message)
        {
            _stringBuilder.Enqueue(Environment.NewLine + DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss.fff") + " ===> " + message);
        }

        private void makeWebRequest(string Uri)
        {
            HttpWebResponse res = null;

            // Make sure that the idle time has elapsed, so that a new 
            // ServicePoint instance is created.
            //_stringBuilder.Enqueue("Sleeping for 2 sec.");
            try
            {
                // Create a request to the passed URI.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Uri);

                _stringBuilder.Enqueue("\nConnecting to " + Uri + " ............");

                // Get the response object.
                res = (HttpWebResponse)req.GetResponse();
                _stringBuilder.Enqueue("Connected.\n");

                ServicePoint currentServicePoint = req.ServicePoint;
                ShowProperties(currentServicePoint);

                // Display new service point properties.
                int currentHashCode = currentServicePoint.GetHashCode();

                _stringBuilder.Enqueue("New service point hashcode: " + currentHashCode);
                _stringBuilder.Enqueue("New service point max idle time: " + currentServicePoint.MaxIdleTime);
                _stringBuilder.Enqueue("New service point is idle since " + currentServicePoint.IdleSince);

            }
            catch (Exception e)
            {
                _stringBuilder.Enqueue("Source : " + e.Source);
                _stringBuilder.Enqueue("Message : " + e.Message);
            }
            finally
            {
                if (res != null)
                    res.Close();
            }
        }

        private void ShowProperties(ServicePoint sp)
        {
            _stringBuilder.Enqueue("Done calling FindServicePoint()...");

            _stringBuilder.Enqueue("Service point hashcode: " + sp.GetHashCode());

            // Display the ServicePoint Internet resource address.
            _stringBuilder.Enqueue(string.Format("Address = {0} ", sp.Address.ToString()));

            // Display the date and time that the ServicePoint was last 
            // connected to a host.
            _stringBuilder.Enqueue("IdleSince = " + sp.IdleSince.ToString());

            // Display the maximum length of time that the ServicePoint instance  
            // is allowed to maintain an idle connection to an Internet  
            // resource before it is recycled for use in another connection.
            _stringBuilder.Enqueue("MaxIdleTime = " + sp.MaxIdleTime);

            _stringBuilder.Enqueue("ConnectionName = " + sp.ConnectionName);

            // Display the maximum number of connections allowed on this 
            // ServicePoint instance.
            _stringBuilder.Enqueue("ConnectionLimit = " + sp.ConnectionLimit);

            // Display the number of connections associated with this 
            // ServicePoint instance.
            _stringBuilder.Enqueue("CurrentConnections = " + sp.CurrentConnections);

            if (sp.Certificate == null)
                _stringBuilder.Enqueue("Certificate = (null)");
            else
                _stringBuilder.Enqueue("Certificate = " + sp.Certificate.ToString());

            if (sp.ClientCertificate == null)
                _stringBuilder.Enqueue("ClientCertificate = (null)");
            else
                _stringBuilder.Enqueue("ClientCertificate = " + sp.ClientCertificate.ToString());

            _stringBuilder.Enqueue("ProtocolVersion = " + sp.ProtocolVersion.ToString());
            _stringBuilder.Enqueue("SupportsPipelining = " + sp.SupportsPipelining);

            _stringBuilder.Enqueue("UseNagleAlgorithm = " + sp.UseNagleAlgorithm.ToString());
            _stringBuilder.Enqueue("Expect 100-continue = " + sp.Expect100Continue.ToString());
        }

        void OverrideTest()
        {
            var client = new HttpClient();
            client.Timeout = new TimeSpan(0, 0, 2);

            _stringBuilder.Enqueue($"ServicePointManager.DefaultConnectionLimit: {ServicePointManager.DefaultConnectionLimit}");

            var uri = new Uri("http://api-test-jobapply.livecareer.com/home/test?waitTime=1&cb=" + DateTime.Now.Ticks);

            _stringBuilder.Enqueue("Before using HttpClient APIs, before changing ConnectionLimit");
            ServicePoint sp = ServicePointManager.FindServicePoint(uri);
            _stringBuilder.Enqueue($"Hashcode Before {sp.GetHashCode()}");
            _stringBuilder.Enqueue($"{uri.AbsoluteUri}, ConnectionLimit (should be {ServicePointManager.DefaultConnectionLimit}): {sp.ConnectionLimit}");
            sp.ConnectionLimit = 2;
            _stringBuilder.Enqueue("Before using HttpClient APIs, after changing ConnectionLimit");
            _stringBuilder.Enqueue($"{uri.AbsoluteUri}, ConnectionLimit (should be 10): {sp.ConnectionLimit}");

            _stringBuilder.Enqueue("Use HttpClient APIs");

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    HttpResponseMessage response = client.GetAsync(uri).Result;
                }
                catch (Exception ex)
                {
                    _stringBuilder.Enqueue($"EXCEPTION : {ex.Message},  INNER_EXCEPTION : {ex.InnerException.Message}");
                }
            }

            _stringBuilder.Enqueue("After using HttpClient APIs");

            sp = ServicePointManager.FindServicePoint(uri);
            _stringBuilder.Enqueue($"Hashcode After {sp.GetHashCode()}");
            // BUG - due to the bug in .NET Framework 4.7.1, the ConnectionLimit for this ServicePoint is changed
            _stringBuilder.Enqueue($"{uri.AbsoluteUri}, ConnectionLimit (should be 10): {sp.ConnectionLimit}");
        }
    }
}